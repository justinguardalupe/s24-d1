// Comparison Query Operators

// $gt (greater than) / $gte (greater than or equal to)

// syntax:
// db.collectionName.find({ field: { $gt: value} });
// db.collectionName.find({ field: { $gte: value} });
db.users.find({"age:" { $gt: 80}});
db.users.find({"age:" { $gt: 70}});

// $lt (less than) / $lte (less than or equal to)
// syntax:
// db.collectionName.find({ field: { $lt: value} });
// db.collectionName.find({ field: { $lte: value} });

// Mini-activity: retrieve all docs where age is less than 50
db.users.find({"age:" { $lt: 76}});

// Mini-activity: retrieve all docs where age is less than or equal to 50
db.users.find({"age:" { $lte: 76}});

// $ne (not equal)
// syntax:
// db.collectionName.find({ field: { $ne: value} });

// Mini-activity: retrieve all docs where age is not equal to 82
db.users.find({ "age": { $ne: 82} });

// $in
// syntax:
// db.collectionName.find({ field: { $in: [value, ...] } });

// Mini-activity: Retrieve all documents where the lastname is on this list "Hawking", "Doe", "Gates".
db.users.find({ "lastName": { $in: ["Hawking", "Doe", "Gates"] } });

// Logical Query Operators

// $or --> either conditions must be true
// syntax:
// db.collectionName.find({ $or: [{ fieldA: valueA }, { fieldB: valueB }] });

// Mini-activity: Retrieve all documents where the firstName is "Neil" or age is 25.
db.users.find({ $or: [{ "firstName": "Neil" }, { "age": 25 }] });

// Mini-activity: Retrieve all documents where the firstName is "Neil" or age is greater than 30.
db.users.find({
	$or: [
		{
			"firstName": "Neil" 
		},
		{
			"age": { $gt: 30}
		}
	]
});

// and --> both conditions must be true
// syntax:
// db.collectionName.find({ $and: [{ fieldA: valueA }, { fieldB: valueB }] });

// Mini-activity: Retrieve all documents where the age is not equal to 82 and not equal to 76.
db.users.find({
	$and: [
		{
			"age": { $ne: 82}
		},
		{
			"age": { $ne: 76}
		}
	]
});


// Field Projections

// Inclusion
// Fields are included to resulting documents
// Syntax:
// db.collectionName.find({ filter }, { projections });
// >> db.collectionName.find({ field: value }, { field: 1, [field: 1, ...] });

// Mini-activity: Retrieve all documents where firstName is not equal to "Jane".
db.users.find({
	"firstName": {
		$ne: "Jane"
		}
	},
	{
		"firstName": 1
	}
);

// By default, _id is always 1. The above is equivalent to:
db.users.find({
	"firstName": {
		$ne: "Jane"
		}
	},
	{
		"_id": 1,
		"firstName": 1
	}
);

// Add more fields to the second argument when needed
db.users.find({
	"firstName": {
		$ne: "Jane"
		}
	},
	{
		"firstName": 1,
		"lastName": 1
	}
);


// Exclusion
// Fields are included to resulting documents
// Syntax:
// db.collectionName.find({ field: value }, { field: 0, [field: 0, ...] });

// Mini-activity: Retrieve all documents where firstName is not equal to "John". Exclude the _id, contact and department fields to the resulting documents.
db.users.find({
	"firstName": {
		$ne: "John"
		}
	},
	{
		"_id": 0,
		"contact": 0,
		"department": 0
	}
);

// Inclusion/exclusion of nested document fields
// Syntax:
/*
	db.collectionName.find({
		field: value
		},
		{
		field.nestedField: 0, [field.nestedField: 1, ...]
	});
*/
/* 
	db.collectionName.find({
		field: value
		},
		{
		field.nestedField: 0, [field.nestedField: 0, ...]
	});
*/

// Mini-activity: Retrieve all documents where age is greater than 75. Include the firstname, lastname and phone number to the resulting documents.
db.users.find({
		"age": {$gt: 75}
		},
		{
		"firstName": 1,
		"lastName": 1,
		"contact.phone": 1
});

// Evaluation Query Operator

// $regex --> patterns
// https://regexone.com/
// syntax:
// db.collectionName.find({ field: $regex: "pattern", $options: '$optionValue' });

// Case-sensitive
// Looks for a name that matches "N"
db.users.find({ "firstName": { $regex: "N" } });

// Looks for a name that matches "n"
db.users.find({ "firstName": { $regex: "n" } });

// Case-insensitive
// $i = case-insensitive option
db.users.find({ "firstName": { $regex: "N", $options: '$i' } });
